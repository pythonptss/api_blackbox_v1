from __future__ import unicode_literals

from django.utils import six
from django.utils.six.moves import StringIO
from django.utils.encoding import force_text
from django.utils.xmlutils import SimplerXMLGenerator
from rest_framework_xml.renderers import XMLRenderer


class ModifiedXMLRenderer(XMLRenderer):

    def __init__(self, item_tag=None,root_tag=None):
        self.item_tag_name = item_tag or "item"
        self.root_tag_name = root_tag or "channel"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Renders `data` into serialized XML.
        """
        if data is None:
            return ''
        stream = StringIO()
        xml = SimplerXMLGenerator(stream, self.charset)
        xml.startDocument()
        self._to_xml(xml, data)
        xml.endDocument()
        return stream.getvalue()

    def _to_xml(self, xml, data):
        if isinstance(data, (list, tuple)):
            for item in data:
                self._to_xml(xml, item)

        elif isinstance(data, dict):
            for key, value in six.iteritems(data):
                xml.startElement(key, {})
                self._to_xml(xml, value)
                xml.endElement(key)

        elif data is None:
            pass

        else:
            xml.characters(force_text(data))