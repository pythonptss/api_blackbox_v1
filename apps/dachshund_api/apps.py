from django.apps import AppConfig


class DachshundApiConfig(AppConfig):
    name = 'dachshund_api'
