from apps.dachshund_api.models import QueryResolver, Imports, GoodInfo, ExciseList, Vatlist, TikList, TikInfo, Notes
from datetime import timedelta, datetime
from django.conf import settings

def delete_tncode(id, data_key):

    try:
        QueryResolver.objects.get(pk=id).delete()
        goodinfo_obj = GoodInfo.objects.get(pk=data_key)
    except GoodInfo.DoesNotExist:
        goodinfo_obj = None
    if goodinfo_obj:
        imports_objs = goodinfo_obj.imports.all()
        for imports_obj in imports_objs:
            imports_obj.valuedetail.remove(*imports_obj.valuedetail.all())
            imports_obj.measure.remove(*imports_obj.measure.all())
        goodinfo_obj.imports.remove(*goodinfo_obj.imports.all())

        exciselist_objs = goodinfo_obj.exciselist.all()
        for exciselist_obj in exciselist_objs:
            exciselist_obj.measure.remove(*exciselist_obj.measure.all())
            exciselist_obj.valuedetailadd.remove(*exciselist_obj.valuedetailadd.all())
        goodinfo_obj.exciselist.remove(*goodinfo_obj.exciselist.all())
        vatlist_objs = goodinfo_obj.vatlist.all()
        for vatlist_obj in vatlist_objs:
            vatlist_obj.valuedetail.remove(*vatlist_obj.valuedetail.all())
            vatlist_obj.valuedetailadd.remove(*vatlist_obj.valuedetailadd.all())
        goodinfo_obj.vatlist.remove(*goodinfo_obj.vatlist.all())
        goodinfo_obj.delete()

    # delete orphans
    all_imports_pks = Imports.objects.values_list('pk', flat=True)
    used_goodinfo_pks = GoodInfo.imports.through.objects.values_list('imports', flat=True)
    Imports.objects.filter(pk__in=list(set(all_imports_pks) - set(used_goodinfo_pks))).delete()

    all_exciselist_pks = ExciseList.objects.values_list('pk', flat=True)
    used_exciselist_pks = GoodInfo.exciselist.through.objects.values_list('exciselist', flat=True)
    ExciseList.objects.filter(pk__in=list(set(all_exciselist_pks) - set(used_exciselist_pks))).delete()

    all_vatlist_pks = Vatlist.objects.values_list('pk', flat=True)
    used_vatlist_pks = GoodInfo.vatlist.through.objects.values_list('vatlist', flat=True)
    Vatlist.objects.filter(pk__in=list(set(all_vatlist_pks) - set(used_vatlist_pks))).delete()

    GoodInfo.objects.get(pk=data_key).delete()

    print("\"tncode\" Deletion request completed.")

def delete_srchstr(id, data_key):

    try:
        QueryResolver.objects.get(pk=id).delete()
        tiklist_obj = TikList.objects.get(pk=data_key)
    except GoodInfo.DoesNotExist:
        tiklist_obj = None

    if tiklist_obj:
        tickinfos = tiklist_obj.tickinfo.all()
        for tickinfo in tickinfos:
            tickinfo.notes.remove(*tickinfo.notes.all())
        tiklist_obj.tickinfo.remove(*tiklist_obj.tickinfo.all())

    # delete orphans tickinfo
    all_tikinfo_pks = TikInfo.objects.values_list('pk', flat=True)
    used_tickinfo_pks = TikList.tickinfo.through.objects.values_list('tikinfo', flat=True)
    TikInfo.objects.filter(pk__in=list(set(all_tikinfo_pks) - set(used_tickinfo_pks))).delete()

    # delete orphans notes
    all_notes_pks = Notes.objects.values_list('pk', flat=True)
    used_tikinfo_pks = TikInfo.notes.through.objects.values_list('notes', flat=True)
    Notes.objects.filter(pk__in=list(set(all_notes_pks) - set(used_tikinfo_pks))).delete()

    # delete main item from
    TikList.objects.get(pk=data_key).delete()

    print("\"srchstr\" Deletion request completed.")
    # Work in progress
    pass

def cache_clear_scheduled_job():
    minutes = int(settings.DACHSHUND_SERVICE["minutes_cache_back"])
    time_back = datetime.now() - timedelta(minutes=minutes)
    records_to_delete = QueryResolver.objects.filter(created_at__lte=time_back).values("id","data_key","query_type")
    for record_to_delete in records_to_delete:
        id = record_to_delete["id"]
        query_type = record_to_delete["query_type"]
        data_key = record_to_delete["data_key"]
        if query_type == 2:
            delete_tncode(id, data_key)
        else:
            delete_srchstr(id, data_key)










