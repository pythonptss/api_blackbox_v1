from django.db import models
from django.utils import timezone

class BaseModel(models.Model):
    objects = models.Manager()
    class Meta:
        abstract = True

# Common Items
class Measure(BaseModel):
    Name = models.CharField(max_length=255, null=True)
    Razm = models.CharField(max_length=500, null=True)


class ValueDetail(BaseModel):
    ValueCount = models.CharField(max_length=255, null=True)
    ValueUnit = models.CharField(max_length=255, null=True)
    ValueCurrency = models.CharField(max_length=255, null=True)
    ValueCount2 = models.CharField(max_length=255, null=True)
    ValueUnit2 = models.CharField(max_length=255, null=True)
    ValueCurrency2 = models.CharField(max_length=255, null=True)


class ValueDetailAdd(models.Model):
    ValueCount = models.CharField(max_length=255, null=True)
    ValueUnit = models.CharField(max_length=255, null=True)


# Vatlist
class Vatlist(BaseModel):
    Value = models.CharField(max_length=255, null=True)
    valuedetail = models.ManyToManyField(ValueDetail, related_name="vatlist_as_valuedetail")
    Condition = models.CharField(max_length=255, null=True)
    Document = models.CharField(max_length=2000, null=True)
    Link = models.CharField(max_length=500, null=True)
    valuedetailadd = models.ManyToManyField(ValueDetailAdd, related_name="vatlist_as_valuedetailadd")


# ExciseList
class ExciseList(BaseModel):
    Value = models.CharField(max_length=255, null=True)
    Condition = models.CharField(max_length=255, null=True)
    Document = models.CharField(max_length=2000, null=True)
    Link = models.CharField(max_length=500, null=True)
    Prim = models.CharField(max_length=500, null=True)
    measure = models.ManyToManyField(Measure, related_name="exciselist_as_measure")
    valuedetailadd = models.ManyToManyField(ValueDetailAdd, related_name="exciselist_as_valuedetailadd")


class Imports(BaseModel):
    Value = models.CharField(max_length=500, null=True)
    valuedetail = models.ManyToManyField(ValueDetail, related_name="imports_as_valuedetail")
    Order = models.CharField(max_length=255, null=True)
    Link =  models.CharField(max_length=500, null=True)
    measure = models.ManyToManyField(Measure, related_name="imports_as_measure")


class GoodInfo(BaseModel):
    Code = models.BigIntegerField(blank=False, null=True)
    Name = models.CharField(max_length=255, null=True)
    Prim = models.CharField(max_length=1000, null=True)
    imports = models.ManyToManyField(Imports, related_name="goodinfo_as_imports")
    exciselist = models.ManyToManyField(ExciseList, related_name="goodinfo_as_exciselist")
    vatlist = models.ManyToManyField(Vatlist, related_name="goodinfo_as_vatlist")


# Search String
class Notes(BaseModel):
    Name = models.CharField(max_length=2500, null=True)

class TikInfo(BaseModel):
    Code = models.BigIntegerField(blank=True, null=True)
    Count = models.IntegerField(null=True)
    notes = models.ManyToManyField(Notes, related_name="tikinfo_as_notes")

class TikList(BaseModel):
    srchstr = models.CharField(max_length=255, null=True)
    tickinfo =  models.ManyToManyField(TikInfo, related_name="tiklist_as_tickinfo")

 # 1 = srchstr and 2 =  tncode
class QueryResolver(BaseModel):
    query_type = models.IntegerField(blank=False, null=True)
    tncode = models.IntegerField(blank=True, null=True)
    country = models.IntegerField(blank=True, null=True)
    date = models.CharField(max_length=255, null=True)
    certificate = models.IntegerField(blank=True, null=True)
    certificate_eav = models.IntegerField(blank=True, null=True)
    srchstr = models.CharField(max_length=255, null=True)
    tnfiltr = models.CharField(max_length=255, null=True)
    page  = models.IntegerField(blank=True, null=True)
    data_key = models.IntegerField(blank=False, null=True) # pk of other tables
    created_at = models.DateTimeField(default=timezone.now)

