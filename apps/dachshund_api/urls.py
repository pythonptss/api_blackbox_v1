from django.urls import include, path, re_path
from . import views

urlpatterns = [
    path('api/v1/tik/xml/',
        views.get_tik_xml.as_view(),
        name='get_xml'
    ),
    path('api/v1/tnved/xml/',
         views.get_tnved_xml.as_view(),
         name='get_xml'
         ),
]