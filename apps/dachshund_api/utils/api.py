import hashlib
import xmltodict
import json
import urllib.request
import xml.etree.ElementTree as ET
from apps.dachshund_api.utils.xml_to_postgresql import ApiResponsePostgreSQLCache
from urllib.parse import quote

class AltaCommunicator:
    RESPONSE_ERROR = ""
    RESPONSE_EMPTY = ""
    RESPONSE_DATA = ""

    def __init__(self, user_name, password, base_api_url_tncode, base_api_url_srchstr, cache_files_path):
        self.username = user_name
        self.password = password
        self.base_api_url_tncode = base_api_url_tncode
        self.base_api_url_srchstr = base_api_url_srchstr
        self.cache_files_path = cache_files_path

    def reframe_xml_and_convert_to_json(self, node_tiklist):

        data_set = {}
        data_set_global = {}

        for tikinfo in node_tiklist:
            tikinfos = tikinfo.findall("./")
            for i in tikinfos:
                if (str(i.tag) == "Notes"):
                    notes = []
                    m = 1
                    for note in i.findall("./"):
                        for name in note.findall("./"):
                            notes.append(name.text)
                    data_set[i.tag] = notes
                    data_set_global[data_set["Code"]] = data_set
                    data_set = {}
                    notes = []
                else:
                    data_set[i.tag] = i.text
        return data_set_global

    def query_srchstr(
            self,
            srchstr,
            tnfiltr,
            page,
            tncode,
            query_type
    ):
        url = self.get_api_url_srchstr(
            srchstr,
            tnfiltr,
            page,
            tncode
        )

        try:
            headers = {'User-Agent': 'Mozilla/5.0'}
            response_obj = urllib.request.urlopen(url)
            response_api = response_obj.read().decode()

            et_xml = ET.fromstring(str(response_api))
            node_tiklist = et_xml.findall("./")
            modified_json = self.reframe_xml_and_convert_to_json(node_tiklist)

            if(len(modified_json) > 0):
                # write xml file
                root = ET.fromstring(response_api)
                tree = ET.ElementTree(root)
                root_element = tree.getroot()
                root_tag = root_element.tag
                if root_tag != "Error":
                    # save xml data in postgres
                    ApiResponsePostgreSQLCache().xml_to_progresql_srchstr(
                        srchstr,
                        tnfiltr,
                        page,
                        tncode,
                        modified_json,
                        query_type
                    )

            # return json of xml
            response = json.loads(json.dumps(xmltodict.parse(response_api)))
            return response
        except Exception as e:
            return {}

    def query_tncode(
            self,
            tncode,
            country,
            date,
            certificate,
            certificate_eav,
            query_type
        ):
        url = self.get_api_url_tncode(
            tncode,
            country,
            date,
            certificate,
            certificate_eav,
            )
        try:
            headers = {'User-Agent': 'Mozilla/5.0'}
            response_obj = urllib.request.urlopen(url)
            response_api = response_obj.read().decode()

            # write xml file
            root = ET.fromstring(response_api)
            tree = ET.ElementTree(root)
            root_element = tree.getroot()
            root_tag = root_element.tag
            if root_tag != "Error":

                # save xml data in postgres
                ApiResponsePostgreSQLCache().xml_to_progresql_tncode(
                    tncode,
                    country,
                    date,
                    certificate,
                    certificate_eav,
                    response_api,
                    query_type
                    )
            # return json of xml
            response = json.loads(json.dumps(xmltodict.parse(response_api)))
            return response
        except Exception as e:
            return {e}

    def get_api_url_tncode(
            self,
            tncode,
            country,
            date,
            certificate,
            certificate_eav
            ):
        secret = self.api_secret(tncode)
        query_string = str(self.base_api_url_tncode) + "?"
        query_string += "tncode=" + str(tncode)
        query_string += "&"
        if country:
            query_string += "country=" + str(country)
            query_string += "&"
        if date:
            query_string += "date=" + str(date)
            query_string += "&"
        if certificate:
            query_string += "certificate=" + str(certificate)
            query_string += "&"
        if certificate_eav:
            query_string += "certificate_eav=" + str(certificate_eav)
            query_string += "&"
        query_string += "login=" + str(self.username)
        query_string += "&"
        query_string += "secret=" + str(secret)
        return query_string

    #arun
    def get_api_url_srchstr(
            self,
            srchstr,
            tnfiltr,
            page,
            tncode
            ):
        secret = self.api_secret(srchstr)
        query_string = str(self.base_api_url_srchstr) + "?"
        query_string += "srchstr=" + quote(srchstr)
        query_string += "&"
        if tnfiltr:
            query_string += "tnfiltr=" + str(tnfiltr)
            query_string += "&"
        if page:
            query_string += "page=" + str(page)
            query_string += "&"
        if tncode:
            query_string += "tncode=" + str(tncode)
            query_string += "&"
        query_string += "login=" + str(self.username)
        query_string += "&"
        query_string += "secret=" + str(secret)
        return query_string


    def isStringEmpty(self, string):
        response = None
        string = str(string)
        if len(string) > 0:
            response = string
        return response

    def convert_to_md5(self, string):
        string = str(string)
        str_hexdigest = None
        if len(string) > 0:
            str_encoded = hashlib.md5(string.encode())
            str_hexdigest = str_encoded.hexdigest()
        return str_hexdigest

    def api_secret(self, key):
        md5_auth_string = None
        key = str(key)
        if len(key) > 0:
            username = self.isStringEmpty(self.username)
            password = self.isStringEmpty(self.password)
            md5_password = self.convert_to_md5(password)
            if md5_password is not None and username is not None and password is not None:
                auth_string = str(key) + ":" + str(username) + ":" + str(md5_password)
                md5_auth_string = self.convert_to_md5(auth_string)
        return md5_auth_string
