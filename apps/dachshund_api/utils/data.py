from apps.dachshund_api.models import QueryResolver
from apps.dachshund_api.utils.postgresql_to_xml import PostgreSQLCacheToXML

class DataCommunicator:

    def __init__(self):
        self.response = None

    #type (search_srchstr)
    @classmethod
    def search_srchstr(
            cls,
            srchstr,
            tnfiltr,
            page,
            tncode,
            query_type
        ):
        argumentos = {}
        argumentos['srchstr'] = str(srchstr) if srchstr else ""
        argumentos['query_type'] = query_type
        if tnfiltr: argumentos['tnfiltr'] = tnfiltr
        if page: argumentos['page'] = page
        if tncode: argumentos['tncode'] = tncode

        try:
            srchstr_data = QueryResolver.objects.filter(**argumentos).values()
        except QueryResolver.DoesNotExist:
            srchstr_data = {}

        return srchstr_data

    # type (search_tncode)
    @classmethod
    def search_tncode(
            cls,
            tncode,
            country,
            date,
            certificate,
            certificate_eav,
            query_type
        ):
        argumentos = {}
        try:
            argumentos['tncode'] = tncode if tncode else ""
        except Exception as e:
            argumentos['tncode'] = ""
        argumentos['query_type'] = int(query_type)
        if country: argumentos['country'] = country
        if date: argumentos['date'] = date
        if certificate: argumentos['certificate'] = certificate
        if certificate_eav: argumentos['certificate_eav'] = certificate_eav

        try:
            srchstr_data = QueryResolver.objects.filter(**argumentos).values()
        except QueryResolver.DoesNotExist:
            srchstr_data = {}
        except Exception as e:
            srchstr_data = {}

        return srchstr_data

    @classmethod
    def get_tncode_cache(cls, data_key):
        postgresql_to_xml_data = PostgreSQLCacheToXML().postgresql_to_xml_tncode(data_key)
        return postgresql_to_xml_data

    @classmethod
    def get_srchstr_cache(cls, data_key):
        postgresql_to_xml_data = PostgreSQLCacheToXML().postgresql_to_xml_srchstr(data_key)
        return postgresql_to_xml_data
