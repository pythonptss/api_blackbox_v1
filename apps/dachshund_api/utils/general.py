from apps.dachshund_api.utils.api import AltaCommunicator
from django.conf import settings
from apps.dachshund_api.utils.data import DataCommunicator as dc

class GeneralUtilities:

    def __init__(self):
        self.base_api_url_srchstr = str(settings.DACHSHUND_SERVICE['base_api_url_srchstr'])
        self.base_api_url_tncode = str(settings.DACHSHUND_SERVICE['base_api_url_tncode'])
        self.username = str(settings.DACHSHUND_SERVICE['username'])
        self.password = str(settings.DACHSHUND_SERVICE['password'])
        self.cache_files_path = str(settings.DACHSHUND_SERVICE['cache_files_path'])
        self.obj = AltaCommunicator(
                self.username,
                self.password,
                self.base_api_url_tncode,
                self.base_api_url_srchstr,
                self.cache_files_path
            )

    def get_srchstr_detail(
            self,
            srchstr,
            tnfiltr,
            page,
            tncode
            ):
            srchstr_data = dc.search_srchstr(
                srchstr,
                tnfiltr,
                page,
                tncode,
                1
            )

            # either get from cache
            # or api and cache it
            cache = None
            if len(srchstr_data) > 0:
                data_key = srchstr_data[0]["data_key"]
                cache = dc.get_srchstr_cache(data_key)
            else:
                cache = self.obj.query_srchstr(
                    srchstr,
                    tnfiltr,
                    page,
                    tncode,
                    query_type = 1
                )
            return cache


    def get_tncode_detail(
            self,
            tncode,
            country,
            date,
            certificate,
            certificate_eav
            ):

            tncode_data = dc.search_tncode(
                tncode,
                country,
                date,
                certificate,
                certificate_eav,
                2
            )

            # either get from cache
            # or api and cache it
            cache = None
            if len(tncode_data) > 0:
                data_key = tncode_data[0]["data_key"]
                cache = dc.get_tncode_cache(data_key)
            else:
                cache = self.obj.query_tncode(
                    tncode,
                    country,
                    date,
                    certificate,
                    certificate_eav,
                    query_type = 2
                )

            return cache