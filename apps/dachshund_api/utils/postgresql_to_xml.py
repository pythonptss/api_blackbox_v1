from apps.dachshund_api.models import GoodInfo, TikList

class PostgreSQLCacheToXML:

    def make_import_node(self, imports_objs):
        import_data = {}
        import_data["Import"] = []
        if (len(imports_objs) > 0):
            imports_obj = imports_objs[0]

            # append imports data
            imports_content = []
            for content in imports_objs.values():
                del content['id']
                imports_content.append(content)
            import_data['Import'].append(imports_content)

            # append value detail data
            valuedetail = {}
            valuedetail["ValueDetail"] = []
            valuedetail_obj = imports_obj.valuedetail.all()
            if (len(valuedetail_obj) > 0):
                valuedetail_content = []
                for content in valuedetail_obj.values():
                    del content['id']
                    content = {k: v for k, v in content.items() if v is not None}
                    valuedetail_content.append(content)
                valuedetail["ValueDetail"].append(valuedetail_content)
                import_data['Import'].append(valuedetail)

            # append measure detail data
            measure = {}
            measure["Measure"] = []
            measure_obj = imports_obj.measure.all()
            if (len(measure_obj) > 0):
                measure_content = []
                for content in measure_obj.values():
                    del content['id']
                    content = {k: v for k, v in content.items() if v is not None}
                    measure_content.append(content)
                measure["Measure"].append(measure_content)
                import_data['Import'].append(measure)

        return import_data

    def make_exciselist_node(self, exciselist_objs):
        exciselist_data = {}
        exciselist_data["Exciselist"] = []
        if (len(exciselist_objs) > 0):
            for exciselist_obj in exciselist_objs.values():
                exciselist_item_obj = exciselist_obj
                if (len(exciselist_item_obj) > 0):
                    exciselist_item_content = {}
                    exciselist_item_content["Excise"] = []
                    del exciselist_item_obj['id']
                    exciselist_item_content["Excise"].append(exciselist_item_obj)
                    exciselist_data["Exciselist"].append(exciselist_item_content)

        return exciselist_data

    def make_vatlist_node(self, vatlist_objs):
        vatlist_data = {}
        vatlist_data["VatList"] = []
        if (len(vatlist_objs) > 0):
            for vatlist_obj in vatlist_objs.values():
                vatlist_item_obj = vatlist_obj
                if (len(vatlist_item_obj) > 0):
                    vatlist_item_content = {}
                    vatlist_item_content["Vat"] = []
                    del vatlist_item_obj['id']
                    vatlist_item_content["Vat"].append(vatlist_item_obj)
                    vatlist_data["VatList"].append(vatlist_item_content)

        return vatlist_data

    def postgresql_to_xml_tncode(self, data_key):

        try:
            goodinfo_obj = GoodInfo.objects.get(pk = data_key)
        except GoodInfo.DoesNotExist:
            goodinfo_obj = None

        data = {}
        if goodinfo_obj:
            data['GoodInfo'] = []
            data['GoodInfo'].append({
                'Code': goodinfo_obj.Code,
                'Name': goodinfo_obj.Name,
                'Prim': goodinfo_obj.Prim
            })
            imports_objs = goodinfo_obj.imports.all()
            exciselist_objs = goodinfo_obj.exciselist.all()
            vatlist_objs = goodinfo_obj.vatlist.all()

            # append import node
            import_data = self.make_import_node(imports_objs)
            if len(import_data) > 0:
                data['GoodInfo'].append(import_data)

            # append exciselist node
            exciselist_data = self.make_exciselist_node(exciselist_objs)
            if len(exciselist_data) > 0:
                data['GoodInfo'].append(exciselist_data)

            # append vatlist node
            vatlist_data = self.make_vatlist_node(vatlist_objs)
            if len(vatlist_data) > 0:
                data['GoodInfo'].append(vatlist_data)

        return data

    def postgresql_to_xml_srchstr(self, data_key):

        try:
            tiklist_obj = TikList.objects.get(pk=data_key)
        except GoodInfo.DoesNotExist:
            tiklist_obj = None

        tiklist = {}
        tiklist['TikList'] = []

        tikinfo = {}
        tikinfo["TikInfo"] = []

        if tiklist_obj:

            tickinfo_objs = tiklist_obj.tickinfo.all()
            for tickinfo_obj in tickinfo_objs:
                item = {}
                item["Code"] = tickinfo_obj.Code
                item["Count"] = tickinfo_obj.Count
                item["Notes"] = []
                for note_obj in tickinfo_obj.notes.all():
                    note = {}
                    note["Note"] = []
                    name = {}
                    name["Name"] = []
                    name["Name"].append(note_obj.Name)
                    note["Note"].append(name)
                    item["Notes"].append(note)
                tikinfo["TikInfo"].append(item)
                tiklist['TikList'].append(tikinfo)
        return tiklist