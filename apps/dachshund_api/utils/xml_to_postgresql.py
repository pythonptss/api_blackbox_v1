import xml.etree.ElementTree as ET
from apps.dachshund_api.models import GoodInfo, Imports, ValueDetail, ExciseList, Measure, Vatlist, QueryResolver, ValueDetailAdd, TikList, TikInfo, Notes

class ApiResponsePostgreSQLCache:

    def add_import_node(self, goodinfo, et_xml):

        # Insert Import Details
        xml_node = et_xml.findall("./Import/")
        mapping = ["ValueDetail", "Measure"]
        split_node = "Value"
        response = self.scrap_node_data(xml_node, split_node, mapping)

        for item in response:
            # Insert Import Value
            imports_obj = Imports(
                Value=item["Value"] if "Value" in item else None,
                Order=item["Order"] if "Order" in item else None,
                Link=item["Link"] if "Link" in item else None
            )
            imports_obj.save()

            # Insert value detail for under import
            value_detail = item["ValueDetail"] if "ValueDetail" in item else {}
            if (len(value_detail) > 0):
                valuedetail_obj = ValueDetail(
                            ValueCount = value_detail["ValueCount"] if "ValueCount" in value_detail else None,
                            ValueUnit=value_detail["ValueUnit"] if "ValueUnit" in value_detail else None,
                            ValueCurrency = value_detail["ValueCurrency"] if "ValueCurrency" in value_detail else None,
                            ValueCount2 = value_detail["ValueCount2"] if "ValueCount2" in value_detail else None,
                            ValueUnit2 = value_detail["ValueUnit2"] if "ValueUnit2" in value_detail else None,
                            ValueCurrency2 = value_detail["ValueCurrency2"] if "ValueCurrency2" in value_detail else None,
                        )
                valuedetail_obj.save()
                imports_obj.valuedetail.add(valuedetail_obj)

            # Insert measure under import
            measure = item["Measure"] if "Measure" in item else {}
            if (len(measure) > 0):
                measure_obj = Measure.objects.create(
                    Name=measure["Name"] if "Name" in measure else None,
                    Razm=measure["Razm"] if "Razm" in measure else None,
                )
                measure_obj.save()
                imports_obj.measure.add(measure_obj)

            # bind with good info object
            goodinfo.imports.add(imports_obj)
            #imports_obj = None

    def add_exciselist_node(self, goodinfo_obj, et_xml):

        # Insert Excise List Details
        xml_node = et_xml.findall("./Exciselist/Excise/")
        mapping = ["Measure", "ValueDetailAdd"]
        split_node = "Value"
        response = self.scrap_node_data(xml_node, split_node, mapping)

        for item in response:
            # Insert Excise list Value
            exciselist_obj = ExciseList(
                Value=item["Value"] if "Value" in item else None,
                Condition=item["Condition"] if "Condition" in item else None,
                Document=item["Document"] if "Document" in item else None,
                Link=item["Link"] if "Link" in item else None
            )
            exciselist_obj.save()

            # Insert measure for under exciselist
            measure = item["Measure"] if "Measure" in item else {}
            if (len(measure) > 0):
                measure_obj = Measure.objects.create(
                    Name=measure["Name"] if "Name" in measure else None,
                    Razm=measure["Razm"] if "Razm" in measure else None,
                )
                measure_obj.save()
                exciselist_obj.measure.add(measure_obj)
                measure_obj = None

            # Insert valuedetailadd for under exciselist
            valuedetailadd = item["ValueDetailAdd"] if "ValueDetailAdd" in item else {}
            if (len(valuedetailadd) > 0):
                valuedetailadd_obj = ValueDetailAdd.objects.create(
                    ValueCount=valuedetailadd["ValueCount"] if "ValueCount" in valuedetailadd else None,
                    ValueUnit=valuedetailadd["ValueUnit"] if "ValueUnit" in valuedetailadd else None,
                )
                valuedetailadd_obj.save()
                exciselist_obj.valuedetailadd.add(valuedetailadd_obj)
                valuedetailadd_obj = None

            goodinfo_obj.exciselist.add(exciselist_obj)
            exciselist_obj = None

    def add_vatlist_node(self, goodinfo_obj, et_xml):
        # Insert VATlist Details
        xml_node = et_xml.findall("./VATlist/VAT/")
        mapping = ["ValueDetail"]
        split_node = "Value"
        response = self.scrap_node_data(xml_node, split_node, mapping)

        for item in response:
            # Insert VATlist Value
            vatlist_obj = Vatlist(
                Value= item["Value"] if "Value" in item else None,
                Condition=item["Condition"] if "Condition" in item else None,
                Document=item["Document"] if "Document" in item else None,
                Link=item["Link"] if "Link" in item else None
            )
            vatlist_obj.save()

            # Insert valuedetail for under VATlist
            value_detail = item["ValueDetail"] if "ValueDetail" in item else {}
            if (len(value_detail) > 0):

                valuedetail_obj = ValueDetail(
                    ValueCount=value_detail["ValueCount"] if "ValueCount" in value_detail else None,
                    ValueUnit=value_detail["ValueUnit"] if "ValueUnit" in value_detail else None,
                    ValueCurrency = value_detail["ValueCurrency"] if "ValueCurrency" in value_detail else None,
                    ValueCount2 = value_detail["ValueCount2"] if "ValueCount2" in value_detail else None,
                    ValueUnit2 = value_detail["ValueUnit2"] if "ValueUnit2" in value_detail else None,
                    ValueCurrency2 = value_detail["ValueCurrency2"] if "ValueCurrency2" in value_detail else None
                )
                valuedetail_obj.save()
                vatlist_obj.valuedetail.add(valuedetail_obj)

                #valuedetail = None

            goodinfo_obj.vatlist.add(vatlist_obj)
            #vatlist_obj = None

    def xml_to_progresql_tncode(
            self,
            tncode,
            country,
            date,
            certificate,
            certificate_eav,
            response_api,
            query_type
            ):

        ### xml insertion in postres database ###

        # read xml
        et_xml = ET.fromstring(str(response_api))
        Code = et_xml.find("Code")
        Name = et_xml.find("Name")
        Prim = et_xml.find("Prim")

        # GetInfo
        goodinfo_obj = GoodInfo.objects.create(
            Code = str(Code.text),
            Name = str(Name.text),
            Prim = str(Prim.text),
        )
        data_key = goodinfo_obj.pk

        self.add_import_node(goodinfo_obj, et_xml)
        self.add_exciselist_node(goodinfo_obj, et_xml)
        self.add_vatlist_node(goodinfo_obj, et_xml)

        try:
            QueryResolver.objects.create(
                query_type = int(query_type),
                tncode = tncode,
                country = country,
                date = date,
                certificate = certificate,
                certificate_eav = certificate_eav,
                data_key = data_key
            )
        except Exception as e:
            pass

        return None

    def xml_to_progresql_srchstr(
            self,
            srchstr,
            tnfiltr,
            page,
            tncode,
            modified_json,
            query_type
            ):

        ### xml insertion in postresql database ###
        if (len(modified_json) > 0):

            tiklist_obj = TikList.objects.create(
                srchstr=srchstr,
            )
            for item in modified_json.items():
                data = item[1]
                tickinfo_obj = TikInfo.objects.create(
                    Code=str(data["Code"]),
                    Count=str(data["Count"])
                )
                # save TikInfo
                for subitem in data:
                    if (subitem == "Notes"):
                        for note in data[subitem]:
                            notes_obj = Notes.objects.create(
                                Name = note,
                            )
                            tickinfo_obj.notes.add(notes_obj)
                if tickinfo_obj:
                    tiklist_obj.tickinfo.add(tickinfo_obj)
                tickinfo_obj = None

            data_key = tiklist_obj.pk

            try:
                QueryResolver.objects.create(
                    query_type = int(query_type),
                    srchstr = srchstr,
                    tnfiltr = tnfiltr,
                    page = page,
                    tncode = tncode,
                    data_key = data_key
                )
            except Exception as e:
                pass

            return None

    """
        - Scrap node data (xml.etree.ElementTree).
    """
    def scrap_node_data(self, xml_node, xml_child_repetitive_node=None, mapping=None):
        if mapping is None:
            mapping = []
        list = []
        dict = {}
        skip_first = 0
        for child in xml_node:
            if xml_child_repetitive_node and str(child.tag) == str(xml_child_repetitive_node):
                if skip_first > 0:
                    list.append(dict)
                    dict = {}
                if child.tag not in mapping:
                    dict[child.tag] = child.text
            else:
                if child.tag in mapping:
                    child_item_len = len(child)
                    if child_item_len > 0:
                        child_dict = {}
                        for i in range(child_item_len):
                            if i < len(child):
                                child_dict[child[i].tag] = child[i].text
                        dict[child.tag] = child_dict
                    child_dict = {}
                else:
                    dict[child.tag] = child.text

            skip_first = skip_first + 1

        if skip_first == len(xml_node):
            list.append(dict)
        elif xml_child_repetitive_node is None:
            list.append(dict)
        elif len(dict) > 1 and len(list) < 1:
            list.append(dict)
        return list