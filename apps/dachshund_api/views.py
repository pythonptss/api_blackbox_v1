from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView
from .ModifiedXMLRenderer import ModifiedXMLRenderer
from apps.dachshund_api.utils.general import GeneralUtilities

class get_tik_xml(ListCreateAPIView):

    def get_renderers(self):
        return [ModifiedXMLRenderer(item_tag="item_tag", root_tag="root_tag")]

    def get(self, request, *args, **kwargs):
        srchstr = request.GET.get('srchstr', None)
        tnfiltr = request.GET.get('tnfiltr', None)
        page = request.GET.get('page', None)
        tncode = request.GET.get('tncode', None)

        response = GeneralUtilities().get_srchstr_detail(
            srchstr,
            tnfiltr,
            page,
            tncode
        )

        return Response(response, status=status.HTTP_200_OK)


class get_tnved_xml(ListCreateAPIView):

    def get_renderers(self):
        return [ModifiedXMLRenderer(item_tag="item_tag", root_tag="root_tag")]

    def get(self, request, *args, **kwargs):
        tncode = request.GET.get('tncode', None)
        country = request.GET.get('country', None)
        date = request.GET.get('date', None)
        certificate = request.GET.get('certificate', None)
        certificate_eav = request.GET.get('certificate_eav', None)

        response = GeneralUtilities().get_tncode_detail(
            tncode,
            country,
            date,
            certificate,
            certificate_eav
        )

        return Response(response, status=status.HTTP_200_OK)